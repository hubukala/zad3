---
author: Hubert Bukała
title: Szczepan Twardoch
subtitle: Pisarz
date: 1.11.2020
theme: Warsaw
output: zad3_prezentacja
---
## Wstęp

Szczepan Twardoch to polski pisarz, publicysta i znawca kultury śląskiej. Urodził się 23 grudnia 1979 roku w Żernicy. Za swoją twórczość otrzymał liczne nagrody i wyróżnienia – Nagrodę Nautilus, Srebrne Wyróżnienie Nagrody Literackiej im. Jerzego Żuławskiego, Paszport „Polityki”, Nagrodę Czytelników Literackiej Nagrody Nobla, czy też Nagrodę Kościelskich.

## Najważniejsze książki


1. "Król"
2. "Królestwo"
3. "Morfina"
4. "Drach"

## Sukces "Morfiny"

Powieść, za którą Szczepan Twardoch otrzymał najwięcej wyróżnień i nominacji, to Morfina. Jej głównym bohaterem jest podporucznik Konstanty Willemann. Jego matką jest Ślązaczka, ojcem zaś Niemiec. Chłopak zostaje wychowany przez matkę w duchu polskości. Konstanty jednak poszukuje swojej tożsamości, zastanawia się kim naprawdę jest – Polakiem czy Niemcem.

Willemann prowadzi hulaszczy tryb życia, uzależniony jest od morfiny. W jego życiu pojawiają się różne kobiety. Spotkania z nimi nie pomagają w odnalezieniu własnej tożsamości, przy każdej z kobiet Konstanty czuje się kimś innym.

Bohater jest wykreowany przez Szczepana Twardocha na człowieka, który posiada słabości, toczy walkę z życiem. Jest to portret małego człowieka uwikłanego w wielką historię.

## Drach

W swojej nowej powieści Twardoch z epickim rozmachem portretuje losy kilku pokoleń śląskiej rodziny, po której przetoczył się walec XX-wiecznej historii. Jednak "Drach" nie pretenduje do bycia wielką epopeją o Śląsku przebraną w kostium powieści historycznej. Ta przejmująca saga rodzinna przypomina bardziej grecką tragedię z jej uniwersalnym przesłaniem o fundamentalnym tragizmie, jaki wyznacza trajektorię każdego żywego istnienia.

Drach wie. Wraz z kilkuletnim Josefem przygląda się świniobiciu. Jest październikowy poranek 1906 roku i, choć chłopiec nie ma o tym pojęcia, ryk zarzynanego zwierzęcia oraz smak wusztzupy powrócą do niego kilkanaście lat później, gdy po zakończeniu wojny będzie powracał na Śląsk.

Nikodem nigdy nie był w wojsku. Jest za to wziętym architektem. Z poprzedniego związku ma pięcioletnią córkę, a po samochodowym wypadku bliznę nad lewym uchem. Właśnie rozstaje się z żoną, by ułożyć sobie życie z inną. Drach jest jednak świadomy, że dziewczyna coraz bardziej wymyka się mężczyźnie.
Między Josefem a Nikodemem piękne, podłe, smutne i ostatecznie tragiczne losy dwóch rodzin; wiek wojen i powstań, śmierci i narodzin, miłości, zdrad i marzeń, które nigdy się nie spełnią. Drach o nich wie. Widzi przeszłość i zna przyszłość. Dla niego wszystko jest teraz…

## Król i Królestwo

Akcję swoich dwóch powieści Szczepan Twardoch osadził w Warszawie. Z pozoru w obu książkach chodzi o to samo miasto. Jednak już po pierwszych stronach „Króla” i „Królestwa” trzeba zweryfikować ten pogląd. Stolica z kart pierwszej książki jest pełna życia, gangsterskich porachunków i politycznych intryg. Zupełnie inna niż ta z jej drugiej części – martwa i zburzona niemal w całości.

Bohaterem nieoczywistym obu dzieł jest Warszawa. W pierwszej książce to jeszcze tętniąca życiem przedwojenna stolica. Wraz z bohaterami odwiedzamy ówczesne salony, gdzie obserwujemy pewne wątki historyczne i historyczne postaci. Obserwujemy biedotę miejską spod znaku Kercelaka – czyli placu Kercelego. Przedwojenne targowisko rozłożone pomiędzy Chłodną i Towarową i dochodzące do Ogrodowej i Leszno było wtedy sercem gminu. Pozwalało zaopatrzyć się we wszystkie produkty codziennego użytku (nie zawsze z legalnych źródeł), a także stracić ostatnie oszczędności w lotnych szulerniach.

„Królestwo” przedstawia już całkiem inną Warszawę. Pokonaną, zrównaną z ziemią, pozbawioną ducha i życia, którym tętniła we wcześniejszej części. Jest to martwe miasto, pogrążone przerażającej, niemal fizycznie odczuwalnej ciszy. Zakłóca ją z rzadka odgłos wystrzałów czy mowy niemieckiej słyszalnej z ust żołnierzy lub z pojedynczych mieszkań, które przetrwały Powstanie. Jakakolwiek namiastka przedwojennego ducha pojawia się tylko w myślach głównych bohaterów, gdy powracają do dni sprzed inwazji lub tuż po niej. Ukazany zostaje wtedy okres pośredni, w którym Niemcy wprowadzają swoje rządy, jednakże miasto nadal funkcjonuje, jego żywot nie został przerwany, lecz wrzucony w nowe ramy, zarówno polityczne, jak i ekonomiczne.Tym bardziej brutalny staje się powrót do rzeczywistości. Stolica, jaką poznajemy wraz z podróżami Ryfki po jedzenie i opał, sprawia wrażenie zamordowanego miasta.

## Oprawa graficzna okładek

![](/Users/hubertbukala/Pictures/Królestwo.jpg)
![](/Users/hubertbukala/Pictures/c700x420.jpg)

## Ekranizacja powieści "Król"

Serial powstał w oparciu o powieść Szczepana Twardocha pod tym samym tytułem. Za jego reżyserię odpowiada Jan P. Matuszyński. Scenariusz to wspólne dzieło Matuszyńskiego i Twardocha. W rolach głównych występują m.in. Michał Żurawski, Kacper Olszewski, Arkadiusz Jakubik, Borys Szyc i Magdalena Boczarska.

„Król“ to osadzona w Warszawie w 1937 roku gangsterska opowieść o przemocy, żądzy władzy i dążeniu do jej utrzymania za wszelką cenę. Historia wzlotów i upadków mafii, która wzbudzała strach i posłuch w stolicy przed wybuchem II wojny światowej. Główny bohater, żydowski gangster-bokser Jakub Szapiro, zostaje wciągnięty w polityczny spisek i gangsterskie porachunki, które mogą przesądzić o losach państwa.

## Nagrody i wyróżnienia

Szczepan Twardoch otrzymał wiele prestiowych nagród za swoją twórczość, były to między innymi:

* Nagroda Nike za powieść "Morfina" - 2013r.
* Paszport Polityki w kategorii Literatura za powieść "Morfina" - 2013r.
* Nagroda Fundacji im. Kościelskich za powieść "Drach" - 2015r.

![](/Users/hubertbukala/Pictures/file.szczepan-twardoch.jpg)

## Średnia ocen książek na podstawie dwóch czołowych portali dla czytelników


| Tytuł         | Lubimyczytac.pl | biblionetka.pl  |
| ------------- |:-------------:| -----:|
| Król |7.8/10|4.91/6|
| Królestwo |7.6/10|4.75/6|
| Morfina |7.3/10|4.64/6|
| Drach   |7.6/20|4.86/6|
| Pokora  |7.8/10|4.70/6|